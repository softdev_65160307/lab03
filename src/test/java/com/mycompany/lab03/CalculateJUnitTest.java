package com.mycompany.lab03;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import com.mycompany.lab03.Lab03;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author supha
 */
public class CalculateJUnitTest {
    
    public CalculateJUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void testAdd_1_2_output_3() {
        int result = Lab03.add(1,2);
        assertEquals(3,result);
    }
    
    @Test
    public void testAdd_2_2_output_4() {
        int result = Lab03.add(2,2);
        assertEquals(4,result);
    }
    
    @Test
    public void testAdd_9_17_output_26() {
        int result = Lab03.add(9,17);
        assertEquals(26,result);
    }

}
